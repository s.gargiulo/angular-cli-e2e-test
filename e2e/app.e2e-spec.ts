import { AngularCliE2eTestPage } from './app.po';

describe('angular-cli-e2e-test App', () => {
  let page: AngularCliE2eTestPage;

  beforeEach(() => {
    page = new AngularCliE2eTestPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
